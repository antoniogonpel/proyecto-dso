/*
 * FUSE: Filesystem in Userspace
 * DSO 2014
 * Cliente que monta carpeta alojada en servidor
 * 
 * Compilarlo con make
 *  
 * uso:  ./cliente [opciones FUSE] punto_de_montaje
 * 
 * Autores:
 * 		Antonio González Pelayo
 * 		Pablo López García
 * 		Iván Fernández Vega
 * 
 * NOTA: No se usan tildes ni caracteres raros para evitar
 * 			incompatibilidades
*/

#define FUSE_USE_VERSION 26
#include <stdlib.h>
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <arpa/inet.h> 
#include <netinet/in.h>
#include <netdb.h>
#include <sys/statvfs.h>

/* Variables globales auxiliares */
int puerto;
char ip [16];
int buf_len = 0;
char netbuf[1024];
char printbuf[257];
struct sockaddr_in serv_addr;


/**
 * serializarEntero serializa el entero pasado
 * por parametro en la cadena pasada por
 * parametro
*/
void serializarEntero(int entero, char * cadena)
{	
	sprintf(cadena,"%016x",entero);	
}

/**
 * conectar abre una conexion con el servidor, si
 * es posible
*/
int conectar()
{
	int sockfd;
	memset(netbuf, '0',sizeof(netbuf));
    if((sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_IP)) < 0)
    {
        error("\n Error : No se puede crear el socket \n");
        return 1;				//Esto debe devolver 1 ????
    } 

    memset(&serv_addr, '0', sizeof(serv_addr)); 
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(puerto); 

    if(inet_pton(AF_INET, ip, &serv_addr.sin_addr)<=0)
    {
        error("\n Ha ocurrido un error en la conversión de la IP a binario.\n");
        return -1;
    } 

    if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       error("\n Error : Fallo de conexion \n");
       return -1;
    } 
    return sockfd;
}

/**
 * desSerializarDatos lee los campos de la estructura stat
 * que recibe del servidor
*/
void desSerializarDatos(struct stat *stbuf, int sockfd)
{		
	/* Variable auxiliar */
	char cadena[1024];	
	
	/* Leemos la estructura campo a campo */   
	read(sockfd, cadena, 1024);	  
	stbuf->st_dev = strtol(cadena, NULL, 16);	 
			
	read(sockfd, cadena, 1024);
	stbuf->st_ino = strtol(cadena, NULL, 16);
		
	read(sockfd, cadena, 1024);
	stbuf->st_mode = strtol(cadena, NULL, 16);      
	
	read(sockfd, cadena, 1024);
	stbuf->st_nlink = strtol(cadena, NULL, 16);      
		  
	read(sockfd, cadena, 1024);
	stbuf->st_uid = strtol(cadena, NULL, 16);
		   
	read(sockfd, cadena, 1024);
	stbuf->st_gid = strtol(cadena, NULL, 16);
		  
	read(sockfd, cadena, 1024);
	stbuf->st_rdev = strtol(cadena, NULL, 16);
		  
	read(sockfd, cadena, 1024);
	stbuf->st_size = strtol(cadena, NULL, 16);
		  
	read(sockfd, cadena, 1024);
	stbuf->st_blksize = strtol(cadena, NULL, 16);
		  
	read(sockfd, cadena, 1024);
	stbuf->st_blocks = strtol(cadena, NULL, 16);
		  
	read(sockfd, cadena, 1024);
	stbuf->st_atime = strtol(cadena, NULL, 16);
		  
	read(sockfd, cadena, 1024);
	stbuf->st_mtime = strtol(cadena, NULL, 16);
		  
	read(sockfd, cadena, 1024);
	stbuf->st_ctime = strtol(cadena, NULL, 16);	
}

/** desSerializarStatv lee del servidor la estructura
 * de tipo statv pasada por parametro
*/
void desSerializarStatv(struct statvfs *statv, int sockfd)
{		
	/* Variable auxiliar */
	char cadena[1024];	
	 
	/* Leemos la estructura campo a campo */       
	read(sockfd, cadena, 1024);	  
	statv->f_bsize = strtol(cadena, NULL, 16);	 
		
	read(sockfd, cadena, 1024);
	statv->f_frsize = strtol(cadena, NULL, 16);
    
	read(sockfd, cadena, 1024);
	statv->f_blocks = strtoul(cadena, NULL, 16);      
	
	read(sockfd, cadena, 1024);
	statv->f_bfree = strtoul(cadena, NULL, 16);      
	  
	read(sockfd, cadena, 1024);
	statv->f_bavail = strtoul(cadena, NULL, 16);
	   
	read(sockfd, cadena, 1024);
	statv->f_files = strtoul(cadena, NULL, 16);
	  
	read(sockfd, cadena, 1024);
	statv->f_ffree = strtoul(cadena, NULL, 16);
	  
	read(sockfd, cadena, 1024);
	statv->f_favail = strtoul(cadena, NULL, 16);
	  
	read(sockfd, cadena, 1024);
	statv->f_fsid = strtol(cadena, NULL, 16);	
	  
	read(sockfd, cadena, 1024);
	statv->f_flag = strtol(cadena, NULL, 16);	 
	  
	read(sockfd, cadena, 1024);
	statv->f_namemax = strtol(cadena, NULL, 16);	 
}

/**
 * mi_getattr obtiene los atributos de un fichero o directorio
 */
static int mi_getattr(const char *path, struct stat *stbuf)
{
	/* Variables auxiliares */
	char cadena[1024];
	int resultado;
	int sockfd;
	
	/* Intentamos conectar, si tenemos exito continuamos */
	sockfd = conectar();		
	if(sockfd != -1)
	{	
		/* Enviamos al servidor la orden LSTAT (0) */
		serializarEntero(0, cadena);					
		write(sockfd, cadena, 1024); 
		
		/* Enviamos al servidor el path */
		strcpy(cadena, path);
		write(sockfd,cadena,1024);
			 		
		/* Reservamos memoria y recibimos la estructura stat */
		memset(stbuf, 0, sizeof(struct stat));		
		desSerializarDatos(stbuf, sockfd);     
     	
     	/* Leemos el resultado de la operacion */
     	read(sockfd, cadena, 1024);					
		resultado = strtol(cadena, NULL, 16);				 
		resultado -=125;
		
		/* Cerramos el socket */
		close(sockfd);  
		
		return resultado;
	}	
	return -ENOTCONN;
}

/**
 * mi_readdir lee las entradas de un directorio
 */
static int mi_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
			 off_t offset, struct fuse_file_info *fi)
{

	(void) offset;
	(void) fi;	

	/* Variables auxiliares */
	unsigned int numFicheros;
	int i, restat;	
	char cadena[1024]; 
	int sockfd;
	
	/* Intentamos conectar, si exito continuamos */
	sockfd = conectar();		
	if(sockfd != -1)	
	{
		/* Enviamos al servidor la orden READDIR (1) */
		serializarEntero(1, cadena);					
		write(sockfd, cadena, 1024); 
		
		/* Enviamos al servidor el path */	
		strcpy(cadena, path);
		write(sockfd,cadena,1024);
		
		/* Leemos del servidor el numero de ficheros */
		read(sockfd, cadena, 1024);
		numFicheros = strtol(cadena, NULL, 16);
		
		/* Leemos del servidor los nombres de fichero y rellenamos el buffer */
		for(i = 0; i < numFicheros; i++)
		{
			read(sockfd, cadena, 1024);		
			filler(buf, cadena, NULL, 0);
		}
		
		/* Recibimos del servidor el resultado de la operacion */
		/* Leemos el tamagno que ha sido posible leer del fichero */
        read(sockfd, cadena, 1024);		
		restat = strtol(cadena, NULL, 16);	
        restat -= 125; 
		
		/* Cerramos el socket */
		close(sockfd);
		
		return restat;
	}
	return -ENOTCONN;	
}

/**
 * mi_open abre fichero pasado por parametro
 */
static int mi_open(const char *path, struct fuse_file_info *fi)
{
	/* Variables auxiliares */
	char cadena[1024];			
	int sockfd;
	
	/* Intentamos conectar, si exito continuamos */
	sockfd = conectar();	
	if(sockfd != -1)
	{
		/* Enviamos al servidor la orden OPEN (2) */
		serializarEntero(2, cadena);					
		write(sockfd, cadena, 1024); 	
		
		/* Enviamos al servidor el path */
		strcpy(cadena, path);
		write(sockfd,cadena,1024);
		
		/* Enviamos al servidor los flags */		
		serializarEntero(fi->flags, cadena);
		write(sockfd, cadena, 1024);
		
		/* Recibimos del servidor el resultado */
     	read(sockfd, cadena, 1024);
		fi->fh = (uint64_t)strtol(cadena, NULL, 16);	
     	fi->fh -= 125;
     	
     	/* Cerramos el socket */
		close(sockfd); 
		
		/* Si el resultado es correcto, devolvemos 0 */
		if(fi->fh > 0) 
		return 0;
	}
	return -ENOTCONN;
}

/**
 * mi_read lee del fichero pasado por parametro
 * con las opciones dadas
 */
static int mi_read(const char *path, char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	/* Variables auxiliares */
    char cadena[1024];
    int sockfd;
    int tamagno;
    int i;

    /* Intentamos conectar, si exito continuamos */
    sockfd = conectar();
    if(sockfd != -1)        
    {
		/* Enviamos al servidor la orden READ (3) */
		serializarEntero(3, cadena);					
		write(sockfd, cadena, 1024); 
        
        /* Limpiamos el buffer */
        memset(buf, 0, sizeof(buf));
        	
        /* Enviamos al servidor el path */
        strcpy(cadena, path);
        write(sockfd, cadena, 1024);
        
     	/* Enviamos el tamagno que queremos leer */        
        serializarEntero(size, cadena);
        write(sockfd, cadena, 1024);  
        
        /* Enviamos el offset */
        serializarEntero(offset, cadena);
        write(sockfd, cadena, 1024);
        
        /* Enviamos el manejador del fichero */        
        serializarEntero(fi->fh, cadena);
        write(sockfd, cadena, 1024);       
        
        /* Leemos el tamagno que ha sido posible leer del fichero */
        read(sockfd, cadena, 1024);		
		tamagno = strtol(cadena, NULL, 16);	
        tamagno -= 125;
                
        /* Leemos del servidor los datos leidos, si no ha habido error */
        if (tamagno > 0)  
        {			
			read(sockfd, buf,tamagno);
		}       
       
        /* Cerramos el socket */
        close(sockfd);
          
		return tamagno;    
    } 
	return -ENOTCONN;
}

/**
 * mi_write escribe en el fichero pasado por parametro
 * con las opciones dadas
 */
static int mi_write(const char *path, const char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	/* Variables auxiliares */
    char cadena[1024];
    int sockfd;
    int tamagno;
    
    /* Intentamos conectar, si exito continuamos */
    sockfd = conectar();
    if(sockfd != -1)        
    {
		/* Enviamos al servidor la orden WRITE (8) */
		serializarEntero(8, cadena);					
		write(sockfd, cadena, 1024); 
        
        /* Enviamos al servidor el path */
        strcpy(cadena, path);
        write(sockfd,cadena, 1024);
        
     	/* Enviamos al servidor el tamagno que queremos escribir */        
        serializarEntero(size, cadena);
        write(sockfd, cadena, 1024);   
        
        /* Enviamos al servidor el offset */
        serializarEntero(offset, cadena);
        write(sockfd, cadena, 1024);
        
        /* Enviamos al servidor el manejador del fichero */        
        serializarEntero(fi->fh, cadena);
        write(sockfd, cadena, 1024);
        
        /* Enviamos al servidor el contenido a escribir */
        write(sockfd, buf,size);
        
        /* Recibimos el resultado de la operacion */
        read(sockfd, cadena, 1024);		
		tamagno = strtol(cadena, NULL, 16);	
        tamagno -= 125;
        
        /* Cerramos el socket */
        close(sockfd);
          
		return tamagno;      
    } 
	return -ENOTCONN;
}

/**
 * mi_mkdir crea el directorio en modo "mode"
 * pasado por parametro
 */
static int mi_mkdir(const char *path, mode_t mode)
{
	/* Variables auxiliares */
	char cadena[1024];	
	int resultado;
	int sockfd;
	
	/* Intentamos conectar, si exito continuamos */
	sockfd = conectar();
    if(sockfd != -1)        
    {	
		/* Enviamos al servidor la orden MKDIR (4) */
		serializarEntero(4, cadena);					
		write(sockfd, cadena, 1024);	
		
		/* Enviamos al servidor el path */
		strcpy(cadena,path);
		write(sockfd, cadena, 1024);
		
		/* Enviamos al servidor el modo */
		serializarEntero(mode, cadena);
		write(sockfd, cadena, 1024);
		
		/* Recibimos el resultado de la operacion */
     	read(sockfd, cadena, 1024);
		resultado = (int)strtol(cadena, NULL, 16);	
     	resultado -=125;
     	
     	/* Cerramos el socket */
		close(sockfd);  
		
		return resultado;
	}	
	return -ENOTCONN;
}

/**
 * mi_unlink elimina el fichero
 * pasado por parametro
 */
static int mi_unlink(const char *path)
{
	/* Variables auxiliares */
    char cadena[1024];
    int sockfd, resultado;
    
    /* Intentamos conectar, si exito continuamos */
    sockfd = conectar();    
    if(sockfd != -1)        
    {
		/* Enviamos al servidor la orden UNLINK (5) */
		serializarEntero(5, cadena);					
		write(sockfd, cadena, 1024);
        
        /* Enviamos el path al servidor */
        strcpy(cadena, path);
        write(sockfd,cadena, 1024);
        
        /* Recibimos del servidor el resultado de la operacion */
		read(sockfd, cadena, 1024);					
		resultado = strtol(cadena, NULL, 16);	 
		resultado -=125;
           
        /* Cerramos el socket */
        close(sockfd);
        
        return resultado;
    }    
    return -ENOTCONN;
}

/**
 * mi_mknod crea un nodo en el modo
 * pasado por parametro
 */
static int mi_mknod(const char *path, mode_t mode, dev_t dev)
{
	/* Variables auxiliares */
    char cadena[1024];
    int sockfd;
    int resultado;
    
    /* Intentamos conectar, si exito continuamos */
    sockfd = conectar();
    if(sockfd != -1)        
    {
		/* Enviamos al servidor la orden MKNOD (6) */
		serializarEntero(6, cadena);					
		write(sockfd, cadena, 1024);
        
        /* Enviamos el path al servidor */
        strcpy(cadena, path);
        write(sockfd,cadena, 1024);
        
        /* Enviamos el modo al servidor */        
        serializarEntero(mode, cadena);	
		write(sockfd, cadena, 1024);
		
		/* Enviamos dev al servidor */		
		serializarEntero(dev, cadena);
		write(sockfd, cadena, 1024);
		
		/* Leemos el resultado de la operacion */
		read(sockfd, cadena, 1024);					
		resultado = strtol(cadena, NULL, 16);	 
		resultado -=125;
		
		/* Cerramos el socket */
        close(sockfd);
        
        return resultado;
    }   
    return -ENOTCONN;    
}

/**
 * mi_rename cambia el nombre del path
 * pasado por parametro
 */ 
static int mi_rename(const char *path, const char *newpath)
{
	/* Variables auxiliares */
	char  cadena[1024] ;
    int sockfd, restat;
    
    /* Intentamos conectar, si exito continuamos */
    sockfd = conectar();    
    if(sockfd != -1)        
    {
		/* Enviamos al servidor la orden RENAME (7) */
		serializarEntero(7, cadena);					
		write(sockfd, cadena, 1024);
        
        /* Enviamos al servidor el path anterior */
		strcpy(cadena, path);
        write(sockfd,cadena, 1024);
        
        /* Enviamos al servidor el nuevo path */
        strcpy(cadena, newpath);
        write(sockfd,cadena, 1024);      
       
        /* Recibimos del servidor el resultado de la operacion */
     	read(sockfd, cadena, 1024);					
		restat = strtol(cadena, NULL, 16);				 
		restat -=125;
		
		/* Cerramos el socket */
		close(sockfd);
		
		return restat;
    }    
    return -ENOTCONN;
}

/**
 * mi_rmdir elimina el directorio
 * pasado por parametro
 */ 
static int mi_rmdir(const char *path)
{
   /* Variables auxiliares */
	char cadena[1024] ;
	int sockfd;
	int restat;
	
	/* Intentamos conectar, si exito continuamos */  
	sockfd = conectar(); 
	if(sockfd != -1)        
    {
		/* Enviamos al servidor la orden RMDIR (9) */
		serializarEntero(9, cadena);					
		write(sockfd, cadena, 1024);
        
        /* Enviamos al servidor el path */
        strcpy(cadena, path);
        write(sockfd,cadena, 1024); 
        
        /* Recibimos del servidor el resultado de la operacion */
     	read(sockfd, cadena, 1024);					
		restat = strtol(cadena, NULL, 16);				 
		restat -=125;
		
		/* Cerramos el socket */
		close(sockfd);
		
		return restat;
    }
    return -ENOTCONN;
}

/**
 * mi_statfs obtiene las estadisticas
 * del sistema de ficheros
 */ 
static int mi_statfs(const char *path, struct statvfs *statv)
{
    /* Variables auxiliares */
    char cadena[1024];
	int resultado;
	int sockfd;
	
	/* Intentamos conectar, si exito continuamos */
	sockfd = conectar();		
	if(sockfd != -1)
	{	
		/* Enviamos al servidor la orden STATFS (10) */
		serializarEntero(10, cadena);					
		write(sockfd, cadena, 1024);
				
		/* Enviamos al servidor el path */
		strcpy(cadena, path);
		write(sockfd,cadena,1024);
			 		
		/* Reservamos memoria y recibimos la estructura statvfs */
		memset(statv, 0, sizeof(struct statvfs));		
		desSerializarStatv(statv, sockfd);     
     	
     	/* Leemos del servidor el resultado de la operacion */
     	read(sockfd, cadena, 1024);					
		resultado = strtol(cadena, NULL, 16);				 
		resultado -=125;
		
		/* Cerramos el socket */
		close(sockfd);  
		return resultado;
	}	
	return -ENOTCONN;
}

/**
 * mi_chmod cambia el modo del fichero pasado
 * por parametro
 */ 
static int mi_chmod(const char *path, mode_t mode)
{
	/* Variables auxiliares */
	char cadena[1024];
	int resultado;
	int sockfd;
	
	/* Intentamos conectar, si exito continuamos */
	sockfd = conectar();		
	if(sockfd != -1)
	{	
		/* Enviamos al servidor la orden CHMOD (11) */
		serializarEntero(11, cadena);					
		write(sockfd, cadena, 1024);
		
		/* Enviamos al servidor el path */
		strcpy(cadena, path);
		write(sockfd,cadena,1024);
		
		/* Enviamos al servidor el modo */      
        serializarEntero(mode, cadena);
		write(sockfd, cadena, 1024);
		
		/* Leemos el resultado de la operacion */
		read(sockfd, cadena, 1024);					
		resultado = strtol(cadena, NULL, 16);				 
		resultado -=125;
		
		/* Cerramos el socket */
		close(sockfd);  
		return resultado;
	}	
	return -ENOTCONN;	
}

/**
 * mi_chown cambia el propietario y/o grupo del fichero
 * pasado por parametro
 */ 
static int mi_chown(const char *path, uid_t uid, gid_t gid)
{
	/* Variables auxiliares */
	char cadena[1024];
	int resultado;
	int sockfd;
	
	/* Intentamos conectar, si exito continuamos */
	sockfd = conectar();			
	if(sockfd != -1)
	{	
		/* Enviamos al servidor la orden CHOWN (12) */
		serializarEntero(12, cadena);					
		write(sockfd, cadena, 1024);
		
		/* Enviamos al servidor el path */
		strcpy(cadena, path);
		write(sockfd,cadena,1024);
		
		/* Enviamos al servidor el uid */      
        serializarEntero(uid, cadena);		
		write(sockfd, cadena, 1024);
		
		/* Enviamos al servidor el gid */
		serializarEntero(gid, cadena);		
		write(sockfd, cadena, 1024);
		
		/* Leemos el resultado de la operacion */
		read(sockfd, cadena, 1024);					
		resultado = strtol(cadena, NULL, 16);				 
		resultado -=125;
		
		/* Cerramos el socket */
		close(sockfd);  
		return resultado;
	}	
	return -ENOTCONN;	
}

/**
 * mi_link crea un enlace duro a un fichero
*/
static int mi_link(const char *path, const char *newpath)
{
   	/* Variables auxiliares */
   	char cadena[1024];
    int resultado;    
    int sockfd;
    
    /* Intentamos conectar, si exito continuamos */
    sockfd = conectar();    
    if(sockfd != -1)        
    {
		/* Enviamos al servidor la orden LINK (13) */
		serializarEntero(13, cadena);					
		write(sockfd, cadena, 1024);
        
        /* Enviamos el path anterior al servidor */
        strcpy(cadena, path);
        write(sockfd,cadena, 1024);
        
        /* Enviamos el nuevo path al servidor */
        strcpy(cadena, newpath);
        write(sockfd,cadena, 1024);
        
        /* Recibimos el resultado */
        read(sockfd, cadena, 1024);					
		resultado = strtol(cadena, NULL, 16);
		resultado -= 125;
		
		/* Cerramos el socket */		
		close(sockfd);  
		return resultado;
	}	
	return -ENOTCONN;
}

/**
 * mi_symlink crea un enlace simbolico
 * a un fichero
*/
static int mi_symlink(const char *path, const char *link)
{
   	/* Variables auxiliares */
   	char cadena[1024];
    int resultado;    
    int sockfd;
    
    /* Intentamos conectar, si exito continuamos */
    sockfd = conectar();    
    if(sockfd != -1)        
    {
		/* Enviamos al servidor la orden SYMLINK (14) */
		serializarEntero(14, cadena);					
		write(sockfd, cadena, 1024);
        
        /* Enviamos el path al servidor */
        strcpy(cadena, path);
        write(sockfd,cadena, 1024);
        
        /* Enviamos el link al servidor */
        strcpy(cadena, link);
        write(sockfd,cadena, 1024);
       
		/* Recibimos el resultado */		
        read(sockfd, cadena, 1024);					
		resultado = strtol(cadena, NULL, 16);
		resultado -= 125;
		
		/* Cerramos el socket */
		close(sockfd);
		return resultado;
    }
    return -ENOTCONN;
}

/**
 * mi_readlink lee un enlace a un fichero
*/
static int mi_readlink(const char *path, char *link, size_t size)
{
   	/* Variables auxiliares */
   	char cadena[1024];
    int resultado = 0;    
    int sockfd;
    
    /* Intentamos conectar, si exito continuamos */
    sockfd = conectar();    
    if(sockfd != -1)        
    {
		/* Enviamos al servidor la orden READLINK (15) */
		serializarEntero(15, cadena);					
		write(sockfd, cadena, 1024);
        
        /* Enviamos el path al servidor */
        strcpy(cadena, path);
        write(sockfd,cadena, 1024);       
        
        /* Enviamos el size al servidor */
        serializarEntero(size, cadena);
        write(sockfd, cadena, 1024);
        
        /* Recibimos el resultado */
        read(sockfd, cadena, 1024);					
		resultado = strtol(cadena, NULL, 16);
		resultado -= 125;
		
		/* Si el resultado es correcto, copiamos el link */
		if(resultado > 0){
			read(sockfd, cadena, 1024);					
			strcpy(link,cadena);
			resultado = 0;
		}
			
		/* Cerramos el socket */
		close(sockfd);
		return resultado;
    }    
    return -ENOTCONN;
}

/**
 * mi_create crea y abre un fichero en el
 * modo pasado por parametro
*/
static int mi_create(const char * path, mode_t mode, struct fuse_file_info * fi)
{
    /* Variables auxiliares */
    char cadena[1024];    
    int resultado = 0;
    
    /* Intentamos conectar, si exito continuamos */
    int sockfd = conectar();
    if(sockfd != -1)        
    {
		/* Enviamos al servidor la orden CREATE (16) */
		sprintf(cadena,"%016x",16);
        write(sockfd, cadena, 1024);
       
		/* Enviamos el path al servidor */
        strcpy(cadena, path);
        write(sockfd,cadena, 1024);
		
		/* Enviamos el modo al servidor */
		serializarEntero(mode, cadena);
		write(sockfd, cadena, 1024);			
		
		/* Recibimos el resultado del servidor */		
		read(sockfd, cadena, 1024);					
		resultado = strtol(cadena, NULL, 16);	 
		resultado -=125;
		
		/* Asignamos el resultado al manejador del fichero */
		fi->fh = resultado;
        close(sockfd);
        
        /* Si hemos tenido exito devolvemos 0 */
        if(fi->fh > 0)
        return 0;
    }    
    return -ENOTCONN;
}

/**
 * mi_truncate cambia el tamagno de un fichero
*/
static int mi_truncate(const char *path, off_t newsize)
{
	/* Variables auxiliares */
	char cadena[1024];
	int resultado = 0;
	
	/* Intentamos conectar, si exito continuamos */
    int sockfd = conectar();  
    if(sockfd != -1)        
    {
		/* Enviamos al servidor la orden TRUNCATE (17) */
		sprintf(cadena,"%016x",17);
        write(sockfd, cadena, 1024);
        
        /* Enviamos el path al servidor */
        strcpy(cadena, path);
        write(sockfd,cadena, 1024);
		
		/* Enviamos el nuevo tamagno al servidor */
		serializarEntero(newsize, cadena);
		write(sockfd, cadena, 1024);			
				
		/* Recibimos el resultado del servidor */	
		read(sockfd, cadena, 1024);					
		resultado = strtol(cadena, NULL, 16);	 
		resultado -=125;
		
		/* Cerramos el socket */
        close(sockfd);
        
		return resultado;
    }
    return -ENOTCONN;	
}
/***********************************
 * operaciones FUSE
 ***********************************
*/
static struct fuse_operations basic_oper = {
	.getattr	= mi_getattr,
	.readdir	= mi_readdir,
	.open		= mi_open,
	.read		= mi_read,
	.write		= mi_write,
	.mkdir		= mi_mkdir,
	.unlink		= mi_unlink,
	.mknod		= mi_mknod,
	.rename		= mi_rename,
	.rmdir		= mi_rmdir,
	.statfs 	= mi_statfs,
	.chmod		= mi_chmod,
	.chown		= mi_chown,
	.link  		= mi_link,
	.symlink 	= mi_symlink,
	.readlink	= mi_readlink,
	.create		= mi_create,
	.truncate	= mi_truncate
};

/***********************************
 * */
int main(int argc, char *argv[])
{	
	/* Variables auxiliares */
	int ipValida = 0;
	int puertoValido = 0;
	
	/* Mensaje de bienvenida */
	printf("\n----------------------------------------\n");
	printf("************BIENVENIDO/A****************\n");
	printf("----------------------------------------\n\n");
	
	/* Mientras no tengamos una ip valida se la estamos pidiendo al usuario */
	while(!ipValida)
	{
		printf("Introduzca la direccion IP del\n");
		printf("servidor al que quiere conectarse -> ");
		scanf("%s", ip);
		if(!inet_aton(ip, NULL))
		{
			printf("\n\n*** ¡ERROR! Direccion IP no valida ***\n\n");
		}else{
			ipValida = 1;
		}
	}
	
	/* Mientras no tengamos un puerto valido se lo estamos pidiendo al usuario */
	while(!puertoValido)
	{
		printf("\nIntroduzca el puerto del\n");
		printf("servidor al que quiere conectarse -> ");
		scanf("%i", &puerto);
		if(puerto >= 0 && puerto <= 65535)
		{
			puertoValido = 1;
		}else{
			printf("\n\n*** ¡ERROR! Puerto no valido ***\n");
		}
	}
	
	/* Establecemos el maximo de lectura de la funcion read en 4096 bytes */
	argv[argc] = "-o";
	argc++;
	argv[argc] = "max_read=4096";
	argc++;
	
	/* Deshabilitamos multithreading */
	argv[argc] = "-s";
	argc++;
	
	/* Llamamos a fuse_main con los parámetros establecidos */
	return fuse_main(argc, argv, &basic_oper, NULL);
}
