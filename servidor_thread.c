/*
 * FUSE: Filesystem in Userspace
 * DSO 2014
 * Servidor que ofrece una carpeta a la que pueden acceder clientes
 * 
 * Compilarlo con make
 *  
 * uso:  ./servidor puerto_para_escuchar
 * 
 * Autores:
 * 		Antonio González Pelayo
 * 		Pablo López García
 * 		Iván Fernández Vega
 * 
 * NOTA: No se usan tildes ni caracteres raros para evitar
 * 			incompatibilidades
*/
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <pthread.h>
#include <sys/stat.h>
#include <dirent.h>
#include "printIP.c"
#include <fcntl.h>
#include <sys/statvfs.h>

#define MAX_FIC_DIR 1024

/* Variables globales */
char path_completo[1024];
int  arraySockets[100];
char path_base[1024];

/* Ordenes soportadas por el servidor */
/*				0		1		2	  3		4		5		6		7	  8		9		10		11		12	  13	14			15       16		17 */
typedef enum {LSTAT, READDIR, OPEN, READ, MKDIR, UNLINK, MKNOD, RENAME, WRITE, RMDIR, STATVFS, CHMOD, CHOWN, LINK, SYMLINK, READLINK, CREATE, TRUNCATE} ordenes;

/**
 * completar_path completa oel path relativo enviado por el cliente
 * y guarda el path completo en la variable path_completo
 */
void completar_path(char *path)
{
    strcpy(path_completo, path_base);
    strcat(path_completo, path);
}

/**
 * desSerializarCadena transforma a entero la cadena pasada
 * como argumento
 */
int desSerializarCadena(char * cadena)
{
	return (int)strtol(cadena, NULL, 16);
}

/**
 * leerFichero lee un fichero que le pide el cliente
 * y le manda el contenido
*/
void leerFichero(int connfd)
{
	/* Variables auxiliares */
    char cadena[1024];
    char * datosFichero;    
    int retstat;
    uint64_t fh;
    off_t offset;
    size_t size;  
    
    /* Leemos el path desde el cliente */
    read(connfd,cadena,1024); 	 
    completar_path(cadena);
    
    /* Leemos el tamagno a leer */
    read(connfd,cadena,1024);
    size = (size_t)strtol(cadena, NULL, 16);
    
    /* Leemos el desplazamiento */	
	read(connfd,cadena,1024);
	offset = (off_t)strtol(cadena, NULL, 16);
	
	/* Leemos el manejador del fichero */
	read(connfd,cadena,1024);
	fh = strtoull(cadena, NULL, 16);
      
    /* Reservamos memoria para los datos a leer */
    datosFichero = malloc(size);  
      
    /* Leemos del fichero */  
	retstat = pread(fh, datosFichero, size, offset);
		
	/* Si hay error no enviamos datos */
	if (retstat < 0){
		retstat = -errno;
		retstat += 125;	
		sprintf(cadena,"%016x", retstat);			
	}else {
		retstat += 125;	
		sprintf(cadena,"%016x", retstat);
		enviarCliente(cadena, connfd);
		retstat -= 125;
		write(connfd, datosFichero, retstat);
	}
	
	/* Liberamos la memoria reservada */
	free(datosFichero);
		
}

/**
 * escribirFichero escribe en el fichero pedido
 * por el cliente los datos que nos manda
*/
void escribirFichero(int connfd)
{
	/* Variables auxiliares */
	char cadena[1024];
    char * datosFichero;    
    int retstat;
    uint64_t fh;
    off_t offset;
    size_t size;   
    
    /* Leemos el path desde el cliente */
    read(connfd,cadena,1024);
    completar_path(cadena);
    
    /* Leemos el tamagno desde el cliente */
    read(connfd,cadena,1024);    
    size = (size_t)strtol(cadena, NULL, 16);
    
    /* Leemos el desplazamiento desde el cliente */
	read(connfd,cadena,1024);	
	offset = (off_t)strtol(cadena, NULL, 16);
	
	/* Leemos el manejador del fichero */
	read(connfd,cadena,1024);	
	fh = strtoull(cadena, NULL, 16);	
	
	/* Reservamos memoria y leemos los datos a escribir desde el cliente */
	datosFichero = malloc(size);
	read(connfd,datosFichero,size); 
      
    /* Escribimos en el fichero*/  
	retstat = pwrite(fh, datosFichero, size, offset);
		
	/* Comprobamos si ha habido error y enviamos el resultado al cliente */
	if (retstat < 0)
	{
			retstat = -errno;	
	}				
	retstat += 125;		
	sprintf(cadena,"%016x",retstat);
	enviarCliente(cadena, connfd);
	
	/* Liberamos la memoria reservada */
	free(datosFichero);
}

/**
 * enviarEstructuraStat manda al cliente la estructura
 * de tipo stat pasada como parametro (usada en lectura
 * de atributos
*/
void enviarEstructuraStat(struct stat *statbuf, int connfd)
{
	/* Variable auxiliar */
	char cadena[1024];
	
	/* Enviamos campo a campo la estructura */
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_dev);
	enviarCliente(cadena, connfd);  
	
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_ino);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_mode);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_nlink);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_uid);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_gid);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_rdev);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_size);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_blksize);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_blocks);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_atime);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_mtime);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statbuf->st_ctime);
	enviarCliente(cadena, connfd); 	
}  

/**
 * enviarStatfs manda al cliente la estructura de tipo
 * statvfs pasada por parametro (usada para lectura
 * de estadisticas)
*/
void enviarStatfs(struct statvfs *statv, int connfd)
{
	/* Variable auxiliar */
	char cadena[1024];

	/* Enviamos al cliente la estructura campo a campo */
	sprintf(cadena,"%016x",(unsigned int)statv->f_bsize);
	enviarCliente(cadena, connfd);  
	
	sprintf(cadena,"%016x",(unsigned int)statv->f_frsize);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statv->f_blocks);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statv->f_bfree);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statv->f_bavail);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statv->f_files);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statv->f_ffree);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statv->f_favail);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statv->f_fsid);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statv->f_flag);
	enviarCliente(cadena, connfd); 
	
	sprintf(cadena,"%016x",(unsigned int)statv->f_namemax);
	enviarCliente(cadena, connfd); 
}  

/**
 * atributos es la funcion llamada cuando el cliente
 * solicita leer atributos de un fichero o directorio
*/
void atributos(int connfd)
{
	/* Variables auxiliares */
	char cadena[1024];
	int retstat;
	struct stat *statbuf;
	
	/* Reservamos memoria para la estructura stat */	
	statbuf = malloc(sizeof(struct stat));  	
	
	/* Leemos el path desde el cliente */	
	read(connfd, cadena, 1024);
	printf("Mostrando la carpeta %s\n", cadena);
	completar_path(cadena);
	
	/* Ejecutamos lstat */
	retstat = lstat(path_completo, statbuf);
	
	/* Enviamos la estructura al cliente */		
	enviarEstructuraStat(statbuf, connfd);
	
	/* Comprobamos si ha habido error y enviamos resultado al cliente */
	if (retstat < 0)
	{
			retstat = -errno;
	}		
	retstat += 125;
	sprintf(cadena,"%016x",retstat);	
	enviarCliente(cadena, connfd);	
}

/**
 * leerDirectorio es llamada cuando el cliente pide
 * leer las entradas que se encuentran en un directorio
*/
void leerDirectorio(int connfd)
{
	/* Variables auxiliares */
	DIR *dir;
	struct dirent *ent;
	char cadena[1024];
	char nombresFicheros[MAX_FIC_DIR][1024];
	int indiceLectura, i, retstat;
	
	/* Inicializamos el indice y resultado */
	indiceLectura = 1;
	retstat = 0;

	/* Leemos el path desde el cliente */
	read(connfd, cadena, 1024);
	completar_path(cadena);
	
	/* Intentamos abrir el directorio */
	if ((dir = opendir (path_completo)) == NULL) 
	{
		retstat = -errno;  
	} else {
		
		/* Mientras nos queden entradas de directorio se agnaden al array */
		while ((ent = readdir (dir)) != NULL)
		{
			strcpy(nombresFicheros[indiceLectura], ent->d_name);
			indiceLectura++;
		}
		
		/* Almacenamos el numero de entradas de directorio en el array (posicion 0) */
		sprintf(cadena,"%016x",indiceLectura - 1);
		strcpy(nombresFicheros[0], cadena);			
		
		/* Enviamos al cliente las entradas de directorio */
		for(i = 0; i < indiceLectura ; i++)
		{
			enviarCliente(nombresFicheros[i], connfd);
		}
		
		/* Cerramos el directorio */			
		closedir(dir);	
	}
	
	/* Enviamos al cliente el resultado de la operacion */		
	retstat += 125;
	sprintf(cadena,"%016x",retstat);	
	enviarCliente(cadena, connfd);				
}

/**
 * borrarDirectorio es llamada cuando el cliente solicita 
 * borrar un directorio
*/
void borrarDirectorio(int connfd)
{
    /* Variables auxiliares */
    int retstat = 0;
    char cadena[1024];
    
    /* Leemos el path desde el cliente */
    read(connfd,cadena,1024);
    completar_path(cadena);

    /* Eliminamos el directorio */
    retstat = rmdir(path_completo);
    
    /* Comprobamos si ha habido error */
    if (retstat < 0)
    {
        retstat = -errno;
    }
    
    /* Enviamos al cliente el resultado de la operacion */		
	retstat += 125;
	sprintf(cadena,"%016x",retstat);	
	enviarCliente(cadena, connfd);	
}

/**
 * abrir es llamada cuando el cliente solicita abrir un path
 * con unos determinados flags
*/
void abrir(int connfd)
{
	/* Variables auxiliares */
	char cadena [1024];
	int flags;
	int fd;
	
	/* Leemos el path desde el cliente */
	read(connfd, cadena, 1024);
	completar_path(cadena);
						
	/* Recibimos los flags */			
	read(connfd, cadena, 1024);
	flags = strtol(cadena, NULL, 16);	 
			
	/* Abrimos */
	fd = open(path_completo, flags);
			
	/* Enviamos el descriptor del fichero */
	sprintf(cadena,"%016x",fd + 125);
	enviarCliente(cadena, connfd);	
}

/**
 * borrarFichero es llamada cuando el cliente
 * solicita borrar un fichero 
*/
void borrarFichero(int connfd)
{
    /* Variables auxiliares */
    int retstat;
    char cadena[1024];
    
    /* Leemos el path desde el cliente */
    read(connfd,cadena,1024);
    completar_path(cadena);

    /* Eliminamos el fichero */
    retstat = unlink(path_completo);
  
	/* Comprobamos si ha habido errores */
	if (retstat < 0)
	{
		retstat = -errno;
	}
	
    /* Enviamos al cliente el resultado de la operacion */		
	retstat += 125;
	sprintf(cadena,"%016x",retstat);	
	enviarCliente(cadena, connfd);
}

/**
 * crearFichero es llamada cuando el cliente solicita
 * crear un fichero nuevo
*/
void crearFichero(int connfd)
{
	/* Variables auxiliares */
	int retstat;
    char cadena[1024];
    mode_t modo;
    dev_t dispositivo;
    
    /* Leemos el path desde el cliente */
    read(connfd,cadena,1024); 
    completar_path(cadena);

	/* Leemos el modo desde el cliente */
    read(connfd,cadena,1024);
    modo = (mode_t)desSerializarCadena(cadena);
    
    /* Leemos el dispositivo desde el cliente */
    read(connfd,cadena,1024);
    dispositivo = (dev_t)desSerializarCadena(cadena);
    
    /* Creamos el nodo */
    retstat = mknod(path_completo, modo, dispositivo);
    
    /* Comprobamos si ha habido errores */
    if (retstat < 0)
    {
		retstat = -errno;
    }
    
    /* Enviamos al cliente el resultado de la operacion */
    retstat += 125;
	sprintf(cadena,"%016x",retstat);	
	enviarCliente(cadena, connfd);	
}

/** 
 * renombrar es llamada cuando el cliente solicita renombrar
 * un fichero o directorio (tambien para mover)
*/
void renombrar(int connfd)
{
	/* Variables auxiliares */
	char cadena[1024];
	char pathAnterior[1024];
	char nuevoPath[1024];
	int retstat;
	
	/* Leemos el path anterior desde el cliente */
	read(connfd,cadena,1024);
    completar_path(cadena);
	strcpy(pathAnterior, path_completo);
	
	/* Leemos el nuevo path desde el cliente */
	read(connfd,cadena,1024);
    completar_path(cadena);
	strcpy(nuevoPath, path_completo);
	
	/* Renombramos */
	retstat = rename(pathAnterior, nuevoPath);
	
	/* Comprobamos si ha habido errores */
    if (retstat < 0)
    {
		retstat = -errno;
    }
    
    /* Enviamos al cliente el resultado de la operacion */
    retstat += 125;
	sprintf(cadena,"%016x",retstat);	
	enviarCliente(cadena, connfd);	
}

/**
 * crearDirectorio es llamada cuando el cliente solicita
 * crear un directorio nuevo
*/
void crearDirectorio(int connfd)
{
	/* Variables auxiliares */
	char cadena[1024];
	mode_t modo;
	int retstat;
	
	/* Leemos el path desde el cliente */		
	read(connfd, cadena, 1024);
	completar_path(cadena);
						
	/* Leemos el modo desde el cliente */		
	read(connfd, cadena, 1024);
	modo = (mode_t)strtol(cadena, NULL, 16);	 
			
	/* Creamos el directorio */
	retstat = mkdir(path_completo, modo);
			
    /* Comprobamos si ha habido errores */
    if (retstat < 0)
    {
		retstat = -errno;
    }
    
    /* Enviamos al cliente el resultado de la operacion */
    retstat += 125;
	sprintf(cadena,"%016x",retstat);	
	enviarCliente(cadena, connfd);				
}

/**
 * estadisticasSistema es llamada cuando el cliente
 * solicita leer las estadisticas del sistema de ficheros
*/
void estadisticasSistema(int connfd)
{
	/* Variables auxiliares */
	char cadena[1024];
	int retstat;
	struct statvfs *statv;	
	
	/* Reservamos memoria para estructura statvfs */
	statv = malloc(sizeof(struct statvfs));  	
	
	/* Leemos el path desde el cliente */
	read(connfd, cadena, 1024);
	completar_path(cadena);
	
	/* Consultamos las estadisticas */
	retstat = statvfs(path_completo, statv);
	
	/* Enviamos la estructura statv */		
	enviarStatfs(statv, connfd);
	
	/* Comprobamos si ha habido error */
	if (retstat < 0)
	{
		retstat = -errno;
	}	
	
	/* Enviamos el resultado al cliente */	
	retstat += 125;	
	sprintf(cadena,"%016x",retstat);	
	enviarCliente(cadena, connfd);
	
	/* Liberamos la memoria reservada */
	free(statv);
}

/**
 * cambiarModo es llamada cuando el cliente solicita
 * cambiar el modo de un path concreto
*/
void cambiarModo(int connfd)
{
	/* Variables auxiliares */
	char cadena[1024];
	mode_t modo;
	int retstat;	
	
	/* Leemos el path desde el cliente */
	read(connfd, cadena, 1024);
	completar_path(cadena);
	
	/* Leemos el modo desde el cliente */
	read(connfd,cadena,1024);
    modo = (mode_t)desSerializarCadena(cadena);
	
	/* Cambiamos el modo */
	retstat = chmod(path_completo, modo);
	
	/* Comprobamos si ha habido errores */		
	if (retstat < 0)
	{
		retstat = -errno;
	}
	
	/* Enviamos el resultado al cliente */		
	retstat += 125;	
	sprintf(cadena,"%016x",retstat);	
	enviarCliente(cadena, connfd);
}

/**
 * cambiarPropietario es llamada cuando el cliente solicita
 * cambiar el propietario y/o grupo de un path concreto
*/
void cambiarPropietario(int connfd)
{
	/* Variables auxiliares */
	char cadena[1024];
	uid_t uid;
	gid_t gid;
	int retstat;	
	
	/* Leemos el path desde el cliente */
	read(connfd, cadena, 1024);
	completar_path(cadena);
	
	/* Leemos el uid desde el cliente */
	read(connfd,cadena,1024); 	 
    uid = (uid_t)desSerializarCadena(cadena);
    
    /* Leemos el gid desde el cliente */
    read(connfd,cadena,1024); 	 
    gid = (gid_t)desSerializarCadena(cadena);
	
	/* Cambiamos grupo/propietario */
	retstat = chown(path_completo, uid, gid);
	
	/* Comprobamos si ha habido errores */		
	if (retstat < 0)
	{
		retstat = -errno;
	}
	
	/* Enviamos el resultado al cliente */		
	retstat += 125;	
	sprintf(cadena,"%016x",retstat);	
	enviarCliente(cadena, connfd);
}

/**
 * crearEnlace es llamada cuando el cliente solicita crear un
 * enlace duro para un fichero
*/
void crearEnlace(int connfd)
{
	/* Variables auxiliares */
	char cadena[1024];
	char path[1024];
	char linkA[1024];
	int res = 0;
	
	/* Leemos el path desde el cliente */
	read(connfd,cadena,1024);
    completar_path(cadena);
	strcpy(path, path_completo);
	
	/* Leemos el enlace a crear desde el cliente */
	read(connfd,cadena,1024);
    completar_path(cadena);
	strcpy(linkA, path_completo);
	
	/* Creamos el enlace */
	res = link(path, linkA);
	
	/* Enviamos el resultado al cliente */
	res += 125;
	sprintf(cadena,"%016x",res);
	enviarCliente(cadena, connfd);	
}

/**
 * crearLinkSym es llamada cuando el cliente solicita
 * crear un enlace simbolico para un fichero
*/
void crearLinkSimb(int connfd)
{    
    /* Variables auxiliares */
    int retstat = 0;
    char fpath[1024];
    char link[1024];
    char cadena[1024];
    
    /* Leemos el path desde el cliente y compensamos con "/" */
    read(connfd,cadena,1024);
    completar_path("/");
	strcpy(fpath, path_completo);
    strcat(fpath, cadena);
    
    /* Leemos el enlace a crear */
    read(connfd,cadena,1024);
    completar_path(cadena);
	strcpy(link, path_completo);
    
	/* Creamos el enlace simbolico */
    retstat = symlink(fpath, link);
     
	/* Enviamos el resultado al cliente */
    retstat += 125;
	sprintf(cadena,"%016x",retstat);
	enviarCliente(cadena, connfd);
}

/**
 * leerLinkSimb es llamada cuando el cliente pide
 * leer un enlace
*/
void leerLinkSimb(int connfd)
{
	/* Variables auxiliares */
    int retstat = 0;
    char fpath[1024];
    char link[1024];
    char cadena[1024];
    size_t size;
    
    /* Leemos el path desde el cliente */    
    read(connfd,cadena,1024);
    completar_path(cadena);
    strcpy(fpath, path_completo);

    /* Leemos el tamagno desde el cliente */
    read(connfd,cadena,1024);
    size = (size_t)strtol(cadena, NULL, 16);
    
	/* Leemos el enlace */
    retstat = readlink(fpath, link, size - 1);
    
	/* Enviamos el resultado al cliente */
	/* Si no ha habido error enviamos el link */
    if (retstat < 0)
    {
        retstat = -errno;
        retstat += 125;        
		sprintf(cadena,"%016x",retstat);
		enviarCliente(cadena, connfd);
    }else {
		link[retstat] = '\0';
		retstat += 125;		
		sprintf(cadena,"%016x",retstat);
		enviarCliente(cadena, connfd);
		
		/* Enviamos el enlace */
		enviarCliente(link, connfd);
    }    
}
/**
 * createFile es llamada cuando el cliente
 * solicita crear un fichero
*/
void createFile(int connfd)
{
	/* Variables auxiliares */
	int retstat = 0;
    char cadena[1024];
    mode_t modo;
    dev_t dispositivo;
    
    /* Leemos el path desde el cliente */
    read(connfd,cadena,1024);
    completar_path(cadena);

	/* Leemos el modo desde el cliente */
    read(connfd,cadena,1024);
    modo = (mode_t)desSerializarCadena(cadena);
    
    /* Creamos */
    retstat = creat(path_completo, modo);
    
    /* Enviamos el resultado al cliente */
    if (retstat < 0)
			retstat = -errno;
    
    retstat += 125;
	sprintf(cadena,"%016x",retstat);	
	enviarCliente(cadena, connfd);
}

/**
 * truncateFile es llamada cuando el cliente solicita
 * cambiar el tamagno de un fichero
*/
void truncateFile(int connfd)
{
	/* Variables auxiliares */
	int retstat = 0;
    char cadena[1024];
    off_t newsize;
    
    /* Leemos el path desde el cliente */
    read(connfd,cadena,1024);
    completar_path(cadena);

	/* Leemos el nuevo tamagno */
    read(connfd,cadena,1024);
    newsize = (mode_t)desSerializarCadena(cadena);
    
    /* Truncamos */
    retstat = truncate(path_completo, newsize);
    
    /* Enviamos el resultado al cliente */
    if (retstat < 0)
			retstat = -errno;
    
    retstat += 125;
	sprintf(cadena,"%016x",retstat);	
	enviarCliente(cadena, connfd);
}

//--------------------------------------------------------------
void ejecutar_comando(char *comando, int connfd)
{
	/* Variable auxiliar */
	ordenes orden;  
   
	/* Desserializamos la orden */
	orden = (ordenes)desSerializarCadena(comando);
    
    /* Mostramos por pantalla el comando recibido */
	printf("Comando recibido: %i\n",  orden);
	
	/* Interpretamos el comando */
	switch(orden)
	{
		case LSTAT:					
			atributos(connfd);
			break;
			
		case READDIR:
			leerDirectorio(connfd);
			break;			
		
		case OPEN:		
			abrir(connfd);
			break;	
			
		case READ:          
			leerFichero(connfd);
			break;
			
		case MKDIR:
			crearDirectorio(connfd);
			break;
		
		case UNLINK:
			borrarFichero(connfd);
			break;
		
		case MKNOD:
			crearFichero(connfd);
			break;
			
		case RENAME:
			renombrar(connfd);
			break;
			
		case WRITE:
			escribirFichero(connfd);
			break;
			
		case RMDIR:
			borrarDirectorio(connfd);
			break;
			
		case STATVFS:
			estadisticasSistema(connfd);
			break;
			
		case CHMOD:
			cambiarModo(connfd);
			break;
			
		case CHOWN:
			cambiarPropietario(connfd);
			break;
		
		case LINK:	
			crearEnlace(connfd);
			break;
		
		case SYMLINK:
			crearLinkSimb(connfd);
			break;
		
		case READLINK:
			leerLinkSimb(connfd);
			break;	
			
		case CREATE:
			createFile(connfd);
			break;
			
		case TRUNCATE:
			truncateFile(connfd);
			break;
			
		default: 
			printf("ERROR: Orden no encontrada\n");
			break;
	}    
}

/**
 * enviarCliente envia los datos enviados
 * por parametro a la conexion pasada tambien
 * por parametro
*/
int enviarCliente(char * datos, int connfd)
{
	write(connfd, datos, 1024); // escribe paquete
	
}

/** atiende es la funcion que se le asigna
 * a cada hebra cuando se crea
*/
void* atiende( void *p)
{
	/* Variables auxiliares */
	int buf_len;
	char buf[1024];
	int connfd, pos;
	connfd=*((int*)p);
	
	/* Leemos el comando mandado por el cliente */
	buf_len = read(connfd,buf,1024);  

	if (buf_len < 0) 
	{ 
		error("ERROR reading from socket");
		exit(-1); 
	}
	printf("thread servidor (%d) LLego %d bytes\n", getpid(),buf_len);
	
	/* Ejecutamos el comando */
	ejecutar_comando(buf, connfd);
	
	/* Cerramos la conexion */
	close(connfd);
	pthread_exit(NULL);
}

/**
 * Programa principal
*/
int main(int argc, char *argv[])
{
	/* Variables auxiliares */
	int listenfd = 0;
	int connfd = 0;
	int buf_len;
	int cli_len;
	int puerto;  
    struct sockaddr_in serv_addr;
    struct sockaddr_in cli_addr; 
    pthread_t tid;
    
    /* Comprobamos que el numero de argumentos es valido */
	if(argc != 2)
    {
        printf("\n Usar: %s <puerto>\n",argv[0]);
        return 1;
    } 
	
	/* Mensaje de bienvenida */
	printf("\n----------------------------------------\n");
	printf("************BIENVENIDO/A****************\n");
	printf("----------------------------------------\n\n");
	
	/* Pedimos el path base que se va a ofrecer a los clientes */
	printf("Por favor, introduzca el path base,\n");
	printf("y pulse ENTER para continuar -> ");
	scanf("%s", path_base);   

	/* Leemos el puerto */
    puerto = (int)strtol(argv[1],NULL,10);

	/* Definimos los parametros del servidor */
    listenfd = socket(AF_INET /* IPv4 */, SOCK_STREAM /* TCP */, IPPROTO_IP /* IP */);
  
	/* Limpiaos la estructura serv_addr */
    memset(&serv_addr, '0', sizeof(serv_addr)); 
    
    /* Usamos IPv4 */
    serv_addr.sin_family = AF_INET;
    
    /* No se sabe la direccion IP */
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    
    /* Definimos el puerto */
    serv_addr.sin_port = htons(puerto); // puerto

	/* Vinculamos el socket */
    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 

	/* Escuchamos y servimos hasta 10 clientes simultaneos */
    listen(listenfd, 10); 
	
	/* Pintamos las direcciones de los interfaces */
    print_addresses(AF_INET);
 
    printf("\n(%d) servicio progr. con threads escuchando en puerto %d\n", getpid(),puerto);
	
	/* Nos quedamos aceptando conexiones de los clientes */	
    while(1)
    {
        memset(&cli_addr, '0', sizeof(cli_addr));
		cli_len = sizeof(cli_addr);
		
		/* Aqui nos quedamos esperando la peticion */  
        connfd = accept(listenfd, (struct sockaddr*) &cli_addr, &cli_len);       
			
		pthread_create(&tid , NULL, atiende, (void *) &connfd);
		pthread_detach(tid);
        printf("servidor (%d) petición atendida\n", getpid());
	}
}
